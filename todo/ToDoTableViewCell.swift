//
//  ToDoTableViewCell.swift
//  todo
//
//  Created by Ken Charles on 2017-01-04.
//  Copyright © 2017 AFX Software Inc. All rights reserved.
//

import UIKit

class ToDoTableViewCell: UITableViewCell {

    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var caption: UITextField!
    @IBOutlet weak var checkboxImage: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        //print("setSelected")
        // Configure the view for the selected state
    }
    

}
