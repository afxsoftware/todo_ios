//
//  ToDoViewController.swift
//  todo
//
//  Created by Ken Charles on 2016-12-29.
//  Copyright © 2016 AFX Software Inc. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ToDoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    var todosAll: [ToDo] = []
    var todosUnCompleted: [ToDo] = []
    var showAll: Bool = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showHide: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        self.showAll = defaults.bool(forKey: "ShowAll")
        
        showHide.selectedSegmentIndex = self.showAll ? 0 : 1
        
        // tableview
        tableView.dataSource = self
        tableView.delegate = self
        
        
        // Firebase
        let userID = FIRAuth.auth()?.currentUser?.uid
        
        FIRDatabase.database().reference().child("todos/" + userID!).observe(FIRDataEventType.childAdded, with: { (snapshot) in
            //print(snapshot)
            
            let todo = ToDo()
            todo.key = snapshot.key

            
            let value = snapshot.value as? NSDictionary
            
            todo.caption = value?["caption"] as? String ?? ""
            todo.done = value?["done"] as? Bool ?? false
            
            self.todosAll.append(todo)
            
            self.tableView.reloadData()
        })
        
        FIRDatabase.database().reference().child("todos/" + userID!).observe(FIRDataEventType.childRemoved, with: { (snapshot) in
            //print(snapshot)
            
            for idx in 0 ..< self.todosAll.count {
                if self.todosAll[idx].key == snapshot.key {
                    self.todosAll.remove(at: idx)
                    break;
                }
            }
            
            self.tableView.reloadData()
        })
        
        FIRDatabase.database().reference().child("todos/" + userID!).observe(FIRDataEventType.childChanged, with: { (snapshot) in
            //print(snapshot)
            
            let value = snapshot.value as? NSDictionary
            
            for idx in 0 ..< self.todosAll.count {
                if self.todosAll[idx].key == snapshot.key {
                    //self.todosAll[idx].caption = snapshot.caption
                    self.todosAll[idx].caption = value?["caption"] as? String ?? ""
                    self.todosAll[idx].done = value?["done"] as? Bool ?? false
                    break;
                }
            }
            
            self.tableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (showAll == true) {
            return todosAll.count
        } else {
            todosUnCompleted = []
            
            for idx in 0 ..< todosAll.count {
                if (todosAll[idx].done == false) {
                    todosUnCompleted.append(todosAll[idx])
                }
            }
            
            return todosUnCompleted.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print(indexPath.row)
        
        let todo: ToDo
        
        if (showAll == true) {
            todo = todosAll[indexPath.row]
        } else {
            todo = todosUnCompleted[indexPath.row]
        }
        
        
        let cell: ToDoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "todoTableViewCell", for: indexPath) as! ToDoTableViewCell

        cell.caption?.text = todo.caption
        cell.caption.tag = indexPath.row
        cell.caption.borderStyle = UITextBorderStyle.none
        
        cell.checkboxImage.tag = indexPath.row

        if (todo.done) {
            //cell.checkboxImage.setImage(UIImage(named: "checked"), for: .normal)
            let image : UIImage = UIImage(named:"checked")!
            cell.myImageView.image = image
        } else {
            //cell.checkboxImage.setImage(UIImage(named: "unchecked"), for: .normal)
            let image : UIImage = UIImage(named:"unchecked")!
            cell.myImageView.image = image
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //print("canEditRowAt")
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            //todosAll.remove(at: indexPath.row)
            let todo: ToDo
            
            if (self.showAll) {
                todo = todosAll[indexPath.row]
            } else {
                todo = todosUnCompleted[indexPath.row]
            }

            let userID = FIRAuth.auth()?.currentUser?.uid
            FIRDatabase.database().reference().child("todos/" + userID!).child(todo.key).removeValue()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected row: \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("Deselected row: \(indexPath.row)")
    }
    
    @IBAction func addItemTapped(_ sender: Any) {
        performSegue(withIdentifier: "addItemSegue", sender: nil)
    }
    
    
    @IBAction func logout(_ sender: Any) {
        print("Logout...")
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            print("Signed Out!")
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        dismiss(animated: true, completion: nil)
    }

//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        var contentOffset:CGPoint = tableView.contentOffset
//        contentOffset.y += 100
//        tableView.contentOffset = contentOffset
//        
//        return true
//    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        saveCaption(index: textField.tag, caption: textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func saveDone(index: Int, status: Bool) {
        //print("Index: \(index), Status: \(status)")
        
        let userID = FIRAuth.auth()?.currentUser?.uid
        
        let todoUpdate = ["done": status]
        
        let key : String
        if (self.showAll) {
            key = todosAll[index].key
        } else {
            key = todosUnCompleted[index].key
        }
        
        FIRDatabase.database().reference().child("todos/" + userID! + "/" + key).updateChildValues(todoUpdate)
    }
    
    func saveCaption(index: Int, caption: String) {
        //print("Index: \(index), Caption: \(caption)")
    
        let userID = FIRAuth.auth()?.currentUser?.uid
        
        let todoUpdate = ["caption": caption]
        
        let key : String
        if (self.showAll) {
            key = todosAll[index].key
        } else {
            key = todosUnCompleted[index].key
        }
        
        FIRDatabase.database().reference().child("todos/" + userID! + "/" + key).updateChildValues(todoUpdate)
    }
    
    @IBAction func showAllButtonTapped(_ sender: UISegmentedControl) {
        //print("Tapped: \(sender.selectedSegmentIndex)")
        
        let defaults = UserDefaults.standard

        if (sender.selectedSegmentIndex == 0) {
            //print("Show")
            self.showAll = true
            
            defaults.set(true, forKey: "ShowAll")
        }
        else {
            //print("Hide")
            self.showAll = false

            defaults.set(false, forKey: "ShowAll")
        }
        
        self.tableView.reloadData()
    }
    
    
    
    @IBAction func checkboxTap(_ sender: UIButton) {
        print("checkboxTap")
        
        
        let todo: ToDo
        
        if (self.showAll) {
            todo = todosAll[sender.tag]
        } else {
            todo = todosUnCompleted[sender.tag]
        }
        
        saveDone(index: sender.tag, status: !todo.done)

        
    }
    
    func tapHandler(_ gestureRecognizer: UITapGestureRecognizer) {
        print("tapHandler!!!!")
    }
    
}























