//
//  ViewController.swift
//  todo
//
//  Created by Ken Charles on 2016-12-28.
//  Copyright © 2016 AFX Software Inc. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if FIRAuth.auth()?.currentUser != nil {
            // User is signed in.
            print("Goto ToDos")
        } else {
            // No user is signed in.
            print("Choose Login/Register")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login(_ sender: Any) {
        print("Login!")
        //performSegue(withIdentifier: "loginSeque", sender: "")
    }
    
    @IBAction func register(_ sender: Any) {
        print("Register!")
        //performSegue(withIdentifier: "registerSeque", sender: "")
    }

}

