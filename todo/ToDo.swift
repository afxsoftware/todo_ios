//
//  ToDo.swift
//  todo
//
//  Created by Ken Charles on 2016-12-30.
//  Copyright © 2016 AFX Software Inc. All rights reserved.
//

import Foundation

class ToDo {
    var key: String = ""
    var caption: String = ""
    var done: Bool = false
}
