//
//  AddItemVC.swift
//  todo
//
//  Created by Ken Charles on 2017-01-03.
//  Copyright © 2017 AFX Software Inc. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase


class AddItemVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var txtNewToDo: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtNewToDo.becomeFirstResponder()
        
    }
    
    @IBAction func addItemTapped(_ sender: Any) {
        print("Add new ToDo: \(txtNewToDo.text!)")
        
        
        // Firebase
        let userID = FIRAuth.auth()?.currentUser?.uid
        
        //FIRDatabase.database().reference().child("todos/" + userID! + "/").setValue(txtNewToDo.text!)
        
        let ref = FIRDatabase.database().reference()
        let key = ref.child("todos/\(userID)/").childByAutoId().key
        
        print("key: \(key)")
        
        let dictionaryTodo = ["caption" : txtNewToDo.text!, "done" : false] as [String : Any]
        
        let newNode = "todos/\(userID!)/\(key)/"
        print("NewNode: \(newNode)")
        
        let childUpdates = [newNode: dictionaryTodo]
        ref.updateChildValues(childUpdates, withCompletionBlock: { (error, ref) -> Void in
            //self.navigationController?.popViewControllerAnimated(true)
        })
        
        txtNewToDo.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addItemTapped(self)
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        txtNewToDo.resignFirstResponder()
    }
    
    
    
}
