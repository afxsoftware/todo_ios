//
//  LoginViewController.swift
//  todo
//
//  Created by Ken Charles on 2016-12-29.
//  Copyright © 2016 AFX Software Inc. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if FIRAuth.auth()?.currentUser != nil {
            performSegue(withIdentifier: "todoSegue", sender: nil)
        } else {
            self.view.isHidden = false
        }
        //self.view.isHidden = false
    }
    
    @IBAction func LoginClick(_ sender: Any) {
        
        FIRAuth.auth()?.signIn(withEmail: email.text!, password: password.text!, completion: { (user, error) in
            if (error != nil) {
                print("We got an error!")
            } else {
                print(user!.email ?? "user not found")
                
                // save to NSProperties
                
                self.performSegue(withIdentifier: "todoSegue", sender: nil)
            }
        })
        
        //navigationController!.popViewController(animated: true)
    }
}
